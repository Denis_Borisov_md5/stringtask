public class Main {

    public static void main(String[] args) {

        String inputString = "aaabbaac";
        StringBuilder outputString = new StringBuilder();

        String[] inputStringToTokens = inputString.split("(?<=(.))(?!\\1)");

        for (String subString : inputStringToTokens){
            if (subString.length() == 1){
                outputString.append(subString.charAt(0));
            }else {
                outputString.append(subString.length()).append(subString.charAt(0));
            }
        }

        System.out.println(outputString);
    }
}
